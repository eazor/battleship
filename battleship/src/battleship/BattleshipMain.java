package battleship;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.stage.Stage;

public class BattleshipMain extends Application {

	private Board p1Board;
	private Board p2Board;
	private Board p1ShootBoard;
	private Board p2ShootBoard;
	private Stage primaryStage = new Stage();
	private Stage secondaryStage = new Stage();
	private Label gameOverviewText = new Label("Platzieren Sie ihre Schiffe!");
	private Label gameOverviewText2 = new Label("Platzieren Sie ihre Schiffe!");
	private Button reset1 = new Button("RESET");
	private Button reset2 = new Button("RESET");
	private GameLogic gameLogic = GameLogic.getInstance();
	private double stage1X;
	private double stage1Y;
	private double stage2X;
	private double stage2Y;

	public void setGameOverviewText(String text) 
	{
		gameOverviewText.setText(text);
		gameOverviewText2.setText(text);
	}

	public Parent createContent(Label label, Button button, Board... board) 
	{
		BorderPane root = new BorderPane();
		root.setPrefSize(300, 700);
		VBox vbox = new VBox(board);
		vbox.setSpacing(30);
		vbox.setAlignment(Pos.CENTER);
		root.setTop(label);
		BorderPane.setMargin(label, new Insets(3.0, 0.0, 0.0, 0.0));
		BorderPane.setAlignment(label, Pos.CENTER);
		label.setFont(Font.font("Arial Black"));
		root.setBottom(button);
		BorderPane.setAlignment(button, Pos.CENTER);
		BorderPane.setMargin(button, new Insets(0.0, 0.0, 5.0, 0.0));
		setButtonHandlers(button);
		root.setCenter(vbox);
		return root;
	}

	private void setButtonHandlers(Button button) {
		button.setOnAction(new EventHandler<ActionEvent>()
		{
		@Override
		public void handle(ActionEvent event) 
		{
			gameLogic.reset();
		
					try 
					{
						stage1X = primaryStage.getX();
						stage1Y = primaryStage.getY();
						stage2X = secondaryStage.getX();
						stage2Y = secondaryStage.getY();
//						System.out.println("1X: " + stage1X + " 1Y: " + stage1Y + " 2X: " + stage2X + " 2Y: " + stage2Y);
						primaryStage.hide();
						secondaryStage.hide();
						prepareHandler();
						start(primaryStage);
						primaryStage.setX(stage1X);
						primaryStage.setY(stage1Y);
						secondaryStage.setX(stage2X);
						secondaryStage.setY(stage2Y);
					} 
					catch (Exception e) 
					{
						e.printStackTrace();
					}
				}
				});
	}

	@Override
	public void start(Stage primaryStage) throws Exception 
	{
		this.primaryStage = primaryStage;
		prepareHandler();
		Scene scene = new Scene(createContent(gameOverviewText, reset1,  p1ShootBoard, p1Board));
		primaryStage.setTitle("Battleships Player 1");
		primaryStage.setScene(scene);
		primaryStage.setResizable(false);
		primaryStage.setX(600);
		primaryStage.setY(100);
//		primaryStage.setOnHidden(e -> Platform.exit());
		primaryStage.show();

		
		Scene scene2 = new Scene(createContent(gameOverviewText2, reset2,  p2ShootBoard, p2Board));
		secondaryStage.setTitle("Battleships Player 2");
		secondaryStage.setScene(scene2);
		secondaryStage.setResizable(false);
		secondaryStage.setX(950);
		secondaryStage.setY(100);
//		primaryStage.setOnHidden(e -> Platform.exit());
		secondaryStage.show();
	}

	private void prepareHandler() 
	{
		PlaceEventHandler handlerP1 = new PlaceEventHandler(true);
		PlaceEventHandler handlerP2 = new PlaceEventHandler(false);
		ShootEventHandler shoothandlerP1 = new ShootEventHandler(true);
		ShootEventHandler shoothandlerP2 = new ShootEventHandler(false);
		handlerP1.setOpponentHandler(handlerP2);
		handlerP2.setOpponentHandler(handlerP1);
		shoothandlerP1.setOpponentHandler(handlerP2);
		shoothandlerP2.setOpponentHandler(handlerP1);
		p1Board = new Board(handlerP1);
		p1ShootBoard = new Board(shoothandlerP1, true);
		p2Board = new Board(handlerP2);
		p2ShootBoard = new Board(shoothandlerP2, false);
		gameLogic.setShootBoards(p1ShootBoard, p2ShootBoard);
		gameLogic.setMain(this);
	}

	public static void main(String[] args) 
	{
		launch(args);
	}

}
