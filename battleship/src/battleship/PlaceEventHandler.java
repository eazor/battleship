package battleship;

import java.util.ArrayList;

import java.util.List;

import battleship.Board.Cell;
import javafx.event.EventHandler;
import javafx.geometry.Point2D;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;

public class PlaceEventHandler implements EventHandler<MouseEvent> {

	public static ArrayList<Cell> shipLocs1 = new ArrayList<>();
	public static ArrayList<Cell> shipLocs2 = new ArrayList<>();

	private PlaceEventHandler opponentHandler;
	private Board board;

	private int shipsToPlace = 5;
	private boolean isPlayer1;

	public PlaceEventHandler(boolean isPlayer1) 
	{
		this.isPlayer1 = isPlayer1;
	}

	public void setOpponentHandler(PlaceEventHandler opponentHandler) 
	{
		this.opponentHandler = opponentHandler;
	}

	public void setBoard(Board board) 
	{
		this.board = board;
	}

	@Override
	public void handle(MouseEvent event) 
	{
		Cell cell = (Cell) event.getSource();

		placeShip(event.getButton() == MouseButton.PRIMARY, cell.x, cell.y);
	}
	
	private void placeShip(boolean vertical, int x, int y) {
		GameLogic gameLogic = GameLogic.getInstance();
		if (isPlayer1) 
		{
			if (placeShipP1(new Ship(shipsToPlace, vertical), x,y)) 
			{
				--shipsToPlace;
			}
		} 
		else 
		{
			if (placeShipP2(new Ship(shipsToPlace, vertical), x,y)) 
			{
				--shipsToPlace;
			}
		}

		if (opponentHandler.isReady() && isReady() && !gameLogic.isRunning()) 
		{
			gameLogic.startGame();
//			System.out.println("Start Game!");
		}
	}

	public boolean isReady() 
	{
		return shipsToPlace == 0;
	}

	public boolean placeShipP1(Ship ship, int x, int y) 
	{
		if (canPlaceShip(ship, x, y)) 
		{
			int length = ship.type;

			if (ship.vertical) 
			{
				for (int i = y; i < y + length; i++) 
				{
					Cell cell = getCell(x, i);
					cell.ship = ship;
					shipLocs1.add(cell);
					cell.setFill(Color.WHITE);
					cell.setStroke(Color.BLUE);
				}
			} 
			else 
			{
				for (int i = x; i < x + length; i++) 
				{
					Cell cell = getCell(i, y);
					cell.ship = ship;
					shipLocs1.add(cell);
					cell.setFill(Color.WHITE);
					cell.setStroke(Color.BLUE);
				}
			}

			return true;
		}

		return false;
	}

	public boolean placeShipP2(Ship ship, int x, int y) 
	{
		if (canPlaceShip(ship, x, y)) 
		{
			int length = ship.type;

			if (ship.vertical) 
			{
				for (int i = y; i < y + length; i++) 
				{
					Cell cell = getCell(x, i);
					cell.ship = ship;
					shipLocs2.add(cell);
					cell.setFill(Color.WHITE);
					cell.setStroke(Color.GREEN);
				}
			} 
			else 
			{
				for (int i = x; i < x + length; i++) 
				{
					Cell cell = getCell(i, y);
					cell.ship = ship;
					shipLocs2.add(cell);
					cell.setFill(Color.WHITE);
					cell.setStroke(Color.GREEN);
				}
			}

			return true;
		}

		return false;
	}

	public Cell getCell(int x, int y) 
	{
		return (Cell) ((HBox) board.rows.getChildren().get(y)).getChildren().get(x);
	}

	private Cell[] getNeighbors(int x, int y) 
	{
		Point2D[] points = new Point2D[] { new Point2D(x - 1, y), new Point2D(x + 1, y), new Point2D(x, y - 1),
				new Point2D(x, y + 1) };

		List<Cell> neighbors = new ArrayList<Cell>();

		for (Point2D p : points) 
		{
			if (isValidPoint(p)) 
			{
				neighbors.add(getCell((int) p.getX(), (int) p.getY()));
			}
		}

		return neighbors.toArray(new Cell[0]);
	}

	private boolean canPlaceShip(Ship ship, int x, int y) 
	{
		int length = ship.type;

		if (ship.vertical) 
		{
			for (int i = y; i < y + length; i++) 
			{
				if (!isValidPoint(x, i))
					return false;

				Cell cell = getCell(x, i);
				if (cell.ship != null)
					return false;

				for (Cell neighbor : getNeighbors(x, i)) {
					if (!isValidPoint(x, i))
						return false;

					if (neighbor.ship != null)
						return false;
				}
			}
		} 
		else {
			for (int i = x; i < x + length; i++) 
			{
				if (!isValidPoint(i, y))
					return false;

				Cell cell = getCell(i, y);
				if (cell.ship != null)
					return false;

				for (Cell neighbor : getNeighbors(i, y)) {
					if (!isValidPoint(i, y))
						return false;

					if (neighbor.ship != null)
						return false;
				}
			}
		}

		return true;
	}

	private boolean isValidPoint(Point2D point) 
	{
		return isValidPoint(point.getX(), point.getY());
	}

	private boolean isValidPoint(double x, double y) 
	{
		return x >= 0 && x < 10 && y >= 0 && y < 10;
	}

	public void showImpact(int x, int y) 
	{
		Cell c = getCell(x, y);
		c.setFill(Color.BLACK);
		if (c.ship != null) 
		{
			c.setFill(Color.RED);
		}

	}
}