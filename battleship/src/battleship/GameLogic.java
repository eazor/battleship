package battleship;
//Singleton

import java.util.ArrayList;

public class GameLogic {

	private static final GameLogic INSTANCE = new GameLogic();
	
	private GameLogic() {
	}
	
	private boolean running = false;
	private boolean turnWatcher = true;
	private BattleshipMain main;
	
	public void setMain(BattleshipMain main) {
		this.main = main;
	}
	
	public static GameLogic getInstance() 
	{
		return INSTANCE;
	}
	
	public boolean isRunning() 
	{
		return running;
	}
	
	public boolean isTurnWatcher() 
	{
		return turnWatcher;
	}
	
	public void startGame() 
	{
		for (Board board : boards) 
		{
			board.initShoot();
		}
		running = true;
		setText("Spieler 1 ist an der Reihe!");
		gameLogic();
	}
	
	public void stopGame() 
	{
		running = false;
	}

	private void gameLogic() {

	}
	
	public void reset() 
	{
		PlaceEventHandler.shipLocs1.clear();
		PlaceEventHandler.shipLocs2.clear();
		setText("Platzieren Sie ihre Schiffe!");
		stopGame();
	}

	public void switchTurnWatcher() 
	{
		turnWatcher= !turnWatcher;
		if (turnWatcher)
		{
			setText("Player 1 ist an der Reihe!");
		}
		else
		{
			setText("Player 2 ist an der Reihe!");
		}
	}
	
	public void setText(String s) {
		if (main!=null)
			main.setGameOverviewText(s);	
	}

	private ArrayList<Board> boards = new ArrayList<>();

	public void setShootBoards(Board... boards)
	{
		for (Board board : boards) 
		{
			this.boards.add(board);
		}
	}
	
}
