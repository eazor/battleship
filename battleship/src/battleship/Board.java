package battleship;

import java.util.ArrayList;

import javafx.event.EventHandler;
import javafx.scene.Parent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

public class Board extends Parent {
	
	public VBox rows = new VBox();
	public int ships = 5;
	private boolean player;

	GameLogic gameLogic = GameLogic.getInstance();

	public Board(ShootEventHandler handler, boolean player) 
	{
		this.player = player;
		initGui(handler);
	}

	public Board(PlaceEventHandler handler) 
	{
		initPlace(handler);
	}

	public void initShoot() 
	{
		if (player)
			initShoot(PlaceEventHandler.shipLocs2);
		else
			initShoot(PlaceEventHandler.shipLocs1);
	}
	

	private void initShoot(ArrayList<Cell> shipLocations) 
	{
		for (int y = 0; y < 10; y++) 
		{
			HBox row = (HBox) rows.getChildren().get(y);
			for (int x = 0; x < 10; x++) 
			{
				Cell c = (Cell) row.getChildren().get(x);
				placeShips(shipLocations, x, y, c);
			}
		}
	}

	private void placeShips(ArrayList<Cell> shipLocations, int x, int y, Cell c) 
	{
		for (Cell cell : shipLocations) 
		{
			if (cell.x == x && cell.y == y) 
			{
				c.ship = cell.ship;
			}
		}
	}

	public void initPlace(PlaceEventHandler handler) 
	{
		handler.setBoard(this);
		initGui(handler);
	}

	private void initGui(EventHandler<MouseEvent> handler) 
	{
		for (int y = 0; y < 10; y++) 
		{
			HBox row = new HBox();
			for (int x = 0; x < 10; x++) 
			{
				Cell c = new Cell(x, y, this);
				c.setOnMouseClicked(handler);
				row.getChildren().add(c);
			}
			rows.getChildren().add(row);
		}
		getChildren().add(rows);
	}

	public class Cell extends Rectangle 
	{
		public int x, y;
		public Ship ship = null;
		public boolean wasShot = false;
		public boolean wasHit = false;

		private Board board;

		public Cell(int x, int y, Board board) 
		{
			super(30, 30);
			this.x = x;
			this.y = y;
			this.board = board;
			setFill(Color.LIGHTGRAY);
			setStroke(Color.BLACK);
		}

		@Override
		public String toString() 
		{
			return "Cell [x=" + x + ", y=" + y + ", wasShot=" + wasShot + " " + super.toString() + "]";
		}

		public boolean shoot() 
		{
			wasShot = true;
			setFill(Color.BLACK);

			if (ship != null) 
			{
				ship.hit();
				wasHit = true;
				setFill(Color.RED);
				if (!ship.isAlive()) 
				{
					board.ships--;
					if (board.ships == 0)
					{
						if (gameLogic.isTurnWatcher())
						{
							gameLogic.setText("Spieler 1 gewinnt!");
							gameLogic.stopGame();
						}
						else
						{
							gameLogic.setText("Spieler 2 gewinnt!");
							gameLogic.stopGame();
						}
					}
				}
				return true;
			}

			return false;
		}
		
	}
}