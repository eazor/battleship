package battleship;


import battleship.Board.Cell;
import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;

public class ShootEventHandler implements EventHandler<MouseEvent> {

	private PlaceEventHandler opponentHandler;

	private boolean isPlayer1;

	public ShootEventHandler(boolean isPlayer1) 
	{
		this.isPlayer1 = isPlayer1;
	}

	@Override
	public void handle(MouseEvent event) 
	{
		GameLogic gameLogic = GameLogic.getInstance();
		if (gameLogic.isRunning() && gameLogic.isTurnWatcher() == isPlayer1) 
		{
			gameAction(event, gameLogic);
		}
	}

	private void gameAction(MouseEvent event, GameLogic gameLogic) {
		Cell cell = (Cell) event.getSource();
		shootAction(gameLogic, cell);
	}

	private void shootAction(GameLogic gameLogic, Cell cell) {
		if (!cell.wasShot)
			cell.shoot();
		opponentHandler.showImpact(cell.x,cell.y);
		if (!(cell.wasHit))
			gameLogic.switchTurnWatcher();
	}

	public void setOpponentHandler(PlaceEventHandler opponentHandler) 
	{
		this.opponentHandler = opponentHandler;
	}
}
